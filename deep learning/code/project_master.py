import matplotlib.pyplot as plt
#%matplotlib inline
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten
import keras.utils as np_utils
import keras
import numpy as np
import random, math

# On some implementations of matplotlib, you may need to change this value
IMAGE_SIZE = 100

def generate_a_drawing(figsize, U, V, noise=0.0):
    fig = plt.figure(figsize=(figsize,figsize))
    ax = plt.subplot(111)
    plt.axis('Off')
    ax.set_xlim(0,figsize)
    ax.set_ylim(0,figsize)
    ax.fill(U, V, "k")
    fig.canvas.draw()
    imdata = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)[::3].astype(np.float32)
    imdata = imdata + noise * np.random.random(imdata.size)
    plt.close(fig)
    return imdata

def generate_a_rectangle(noise=0.0, free_location=False):
    figsize = 1.0    
    U = np.zeros(4)
    V = np.zeros(4)
    if free_location:
        corners = np.random.random(4)
        top = max(corners[0], corners[1])
        bottom = min(corners[0], corners[1])
        left = min(corners[2], corners[3])
        right = max(corners[2], corners[3])
    else:
        side = (0.3 + 0.7 * np.random.random()) * figsize
        top = figsize/2 + side/2
        bottom = figsize/2 - side/2
        left = bottom
        right = top
    U[0] = U[1] = top
    U[2] = U[3] = bottom
    V[0] = V[3] = left
    V[1] = V[2] = right
    return generate_a_drawing(figsize, U, V, noise)


def generate_a_disk(noise=0.0, free_location=False):
    figsize = 1.0
    if free_location:
        center = np.random.random(2)
    else:
        center = (figsize/2, figsize/2)
    radius = (0.3 + 0.7 * np.random.random()) * figsize/2
    N = 50
    U = np.zeros(N)
    V = np.zeros(N)
    i = 0
    for t in np.linspace(0, 2*np.pi, N):
        U[i] = center[0] + np.cos(t) * radius
        V[i] = center[1] + np.sin(t) * radius
        i = i + 1
    return generate_a_drawing(figsize, U, V, noise)

def generate_a_triangle(noise=0.0, free_location=False):
    figsize = 1.0
    if free_location:
        U = np.random.random(3)
        V = np.random.random(3)
    else:
        size = (0.3 + 0.7 * np.random.random())*figsize/2
        middle = figsize/2
        U = (middle, middle+size, middle-size)
        V = (middle+size, middle-size, middle-size)
    imdata = generate_a_drawing(figsize, U, V, noise)
    return [imdata, [U[0], V[0], U[1], V[1], U[2], V[2]]]

def generate_dataset_classification(nb_samples, noise=0.0, free_location=False):
    # Getting im_size:
    im_size = generate_a_rectangle().shape[0]
    X = np.zeros([nb_samples,im_size])
    Y = np.zeros(nb_samples)
    print('Creating data:')
    for i in range(nb_samples):
        if i % 10 == 0:
            print(i)
        category = np.random.randint(3)
        if category == 0:
            X[i] = generate_a_rectangle(noise, free_location)
        elif category == 1: 
            X[i] = generate_a_disk(noise, free_location)
        else:
            [X[i], V] = generate_a_triangle(noise, free_location)
        Y[i] = category
    X = (X + noise) / (255 + 2 * noise)
    return [X, Y]

def generate_test_set_classification():
    np.random.seed(42)
    [X_test, Y_test] = generate_dataset_classification(300, 20, True)
    Y_test = np_utils.to_categorical(Y_test, 3) 
    return [X_test, Y_test]

def generate_dataset_regression(nb_samples, noise=0.0):
    # Getting im_size:
    im_size = generate_a_triangle()[0].shape[0]
    X = np.zeros([nb_samples,im_size])
    Y = np.zeros([nb_samples, 6])
    print('Creating data:')
    for i in range(nb_samples):
        if i % 10 == 0:
            print(i)
        [X[i], Y[i]] = generate_a_triangle(noise, True)
    X = (X + noise) / (255 + 2 * noise)
    return [X, Y]

import matplotlib.patches as patches

def visualize_prediction(x, y):
    fig, ax = plt.subplots(figsize=(5, 5))
    I = x.reshape((IMAGE_SIZE,IMAGE_SIZE))
    ax.imshow(I, extent=[-0.15,1.15,-0.15,1.15],cmap='gray')
    ax.set_xlim([0,1])
    ax.set_ylim([0,1])

    xy = y.reshape(3,2)
    tri = patches.Polygon(xy, closed=True, fill = False, edgecolor = 'r', linewidth = 5, alpha = 0.5)
    ax.add_patch(tri)

    plt.show()

def generate_test_set_regression():
    np.random.seed(42)
    [X_test, Y_test] = generate_dataset_regression(300, 0)
    return [X_test, Y_test]

def calcTriangle(Y):
    data = [[Y[0],Y[1], math.sqrt(Y[0]**2 + Y[1]**2)],
        [Y[2],Y[3], math.sqrt(Y[2]**2 + Y[3]**2)],
        [Y[4],Y[5], math.sqrt(Y[4]**2 + Y[5]**2)]]
    triangle = np.array([data[0][0], data[0][1], data[1][0], data[1][1], data[2][0], data[2][1]])
    return triangle

def simple_classification():
    ''' Entrainement '''
    [X_train, Y_train] = generate_dataset_classification(1000, 20)
    y_train = np_utils.to_categorical(Y_train)
    #print(y_train)
    #print(X_train.shape[1])
    
    model = Sequential()
    model.add(Dense(3, input_shape=(X_train.shape[1],), activation='softmax'))
    adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(optimizer="adam", loss="categorical_crossentropy" , metrics=['accuracy'])
    model.fit(X_train, y_train, epochs=100, batch_size=32)

    ''' Test '''
    [X_test, Y_test] = generate_test_set_classification()
    loss, metrics = model.evaluate(X_test, Y_test, batch_size=32, verbose=1)
    print(loss)
    print(metrics)

    ''' 3. Visualization of the Solution '''
    X_test = generate_a_disk(10)
    plt.imshow(X_test.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    X_test = X_test.reshape(1, X_test.shape[0])
    predict1 = model.predict(X_test, verbose=1)

    [X_test2, v] = generate_a_triangle(20, False)
    plt.imshow(X_test2.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    X_test2 = X_test2.reshape(1, X_test2.shape[0])
    predict2 = model.predict(X_test2, verbose=1)
    
    X_test3 = generate_a_rectangle(10, True)
    plt.imshow(X_test3.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    X_test3 = X_test3.reshape(1, X_test3.shape[0])
    predict3 = model.predict(X_test3, verbose=1)
    
    [W1, W2] = model.get_weights()
    w1_0 = W1[:,0]
    w1_1 = W1[:,1]
    w1_2 = W1[:,2]

    print(predict1)
    print(predict2)
    print(predict3)
    
    plt.imshow(w1_0.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    plt.show()
    plt.imshow(w1_1.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    plt.show()
    plt.imshow(w1_2.reshape(IMAGE_SIZE,IMAGE_SIZE), cmap='gray')
    plt.show()

def more_difficult_classification():
    ''' Entrainement '''
    [X_train, Y_train] = generate_dataset_classification(500, 5, True)
    X_train = X_train.reshape(len(X_train), IMAGE_SIZE, IMAGE_SIZE, 1)
    y_train = np_utils.to_categorical(Y_train)
        
    [X_test, Y_test] = generate_test_set_classification()
    X_test = X_test.reshape(len(X_test), IMAGE_SIZE, IMAGE_SIZE, 1)

    model = Sequential()
    #model.add(Conv2D(filters=16, kernel_size=(3,3), padding="same", activation="relu", input_shape=(IMAGE_SIZE,IMAGE_SIZE,1)))
    model.add(Conv2D(filters=16, kernel_size=(5,5), padding="same", activation="relu", input_shape=(IMAGE_SIZE,IMAGE_SIZE,1)))
    model.add(MaxPooling2D(pool_size=(2,2)))
    #model.add(Conv2D(filters=16, kernel_size=(3,3), padding="same", activation="relu", input_shape=(IMAGE_SIZE,IMAGE_SIZE,1)))
    model.add(Conv2D(filters=16, kernel_size=(5,5), padding="same", activation="relu", input_shape=(IMAGE_SIZE,IMAGE_SIZE,1)))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(512, input_shape=(X_train.shape[1],), activation='relu'))
    model.add(Dense(128, input_shape=(X_train.shape[1],), activation='relu'))
    model.add(Dense(3, input_shape=(X_train.shape[1],), activation='softmax'))
    model.compile(optimizer="adam", loss="categorical_crossentropy" , metrics=['accuracy'])
    model.fit(X_train, y_train, epochs=10, batch_size=32, validation_data=(X_test, Y_test))

    loss, metrics = model.evaluate(X_test, Y_test, batch_size=32, verbose=1)
    print(loss)
    print(metrics)

def regression():
    [X_train, Y_train] = generate_dataset_regression(300, 0)
    [X_test, Y_test] = generate_test_set_regression()
    
    y_train = np.zeros([len(X_train), 6])
    y_test = np.zeros([len(X_test), 6])
    for i in range(len(X_train)):
        y_train[i] = calcTriangle(Y_train[i])
    for i in range(len(X_test)):
        y_test[i] = calcTriangle(Y_test[i])

    x_train = X_train.reshape(len(X_train), IMAGE_SIZE, IMAGE_SIZE, 1)
    x_test = X_test.reshape(len(X_test), IMAGE_SIZE, IMAGE_SIZE, 1)

    model = Sequential()
    model.add(Conv2D(filters=32, activation="relu", input_shape=(IMAGE_SIZE, IMAGE_SIZE, 1), kernel_size=(3, 3), padding="same"))
    model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))
    model.add(Conv2D(32, (3, 3), activation="relu", padding="same"))
    model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))
    model.add(Flatten())
    model.add(Dense(6))
    model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
    model.fit(x_train, y_train, epochs=10, batch_size=32, validation_data=(x_test, y_test))
    
    #check only 20 figures
    for i in range(20):
        j = random.randint(0,300)
        visualize_prediction(X_test[j], Y_test[j])
    
#simple_classification()
#more_difficult_classification()
regression()