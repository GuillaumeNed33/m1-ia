# -*- coding: utf-8 -*-

import time
import Tictactoe 
from random import randint,choice

def RandomMove(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles'''
    return choice(b.legal_moves())

def deroulementRandom(b):
    '''Effectue un déroulement aléatoire du jeu de morpion.'''
    print("----------")
    print(b)
    if b.is_game_over():
        res = getresult(b)
        if res == 1:
            print("Victoire de X")
        elif res == -1:
            print("Victoire de O")
        else:
            print("Egalité")
        return
    b.push(RandomMove(b))
    deroulementRandom(b)
    b.pop()


def getresult(b):
    '''Fonction qui évalue la victoire (ou non) en tant que X. Renvoie 1 pour victoire, 0 pour 
       égalité et -1 pour défaite. '''
    if b.result() == b._X:
        return 1
    elif b.result() == b._O:
        return -1
    else:
        return 0

def allGames(b):
    '''Fonction explore toutes les parties possibles au Morpion. '''
    global nbGames, nbNodes, winX, winO, noWin
    nbNodes += 1
    if b.is_game_over():
        nbGames += 1
        res = getresult(b)
        if res == 1:
            winX += 1
        elif res == -1:
            winO += 1
        else:
            noWin += 1
        return
    for m in b.legal_moves():
        b.push(m)
        allGames(b)
        b.pop()

def MiniMax(b):
    global nbGames
    if b.is_game_over():
        nbGames += 1
        return getresult(b)
    best = -10
    for m in b.legal_moves():
        b.push(m)
        best = max(best, MiniMin(b))
        b.pop()
    return best

def MiniMin(b):
    global nbGames
    if b.is_game_over():
        nbGames += 1
        return getresult(b)
    worst = 10
    for m in b.legal_moves():
        b.push(m)
        worst = min(worst, MiniMax(b))
        b.pop()
    return worst

board = Tictactoe.Board()
nbGames = 0
nbNodes = 0
winX = 0
winO = 0
noWin = 0
bestCpt = MiniMax(board)
worstCpt = MiniMin(board)

### Deroulement d'une partie aléatoire
#deroulementRandom(board)
#allGames(board) # environ 4sec (3.17) d'execution
print("Nombre de parties : " + str(nbGames))
print("Nombre de noeuds : " + str(nbNodes))
print("Vicoire de X : " + str(winX))
print("Victoire de 0 : " + str(winO))
print("Egalité : " + str(noWin))
print("Best : " + str(bestCpt))
print("Worst : " + str(worstCpt))
#print("Apres le match, chaque coup est défait (grâce aux pop()): on retrouve le plateau de départ :")
#print(board)
#print(board)

