# -*- coding: utf-8 -*-

import time
import chess
from random import randint, choice

############################################
#############    VARIABLES   ###############
############################################
board = chess.Board()
nbGames = 0
nbNodes = 0
PROFONDEUR_MAX = 3
MAX_REFLEXION_TIME = 10
deadLineReflexion = 0
valpieces = {
    '.': 0,
    'P': 1, 'p': -1,
    'R': 5, 'r': 5,
    'N': 3, 'n': -3,
    'B': 3, 'b': -3,
    'Q': 9, 'q': -9,
    'K': 200, 'k': -200,
    }

############################################
#############    FUNCTIONS   ###############
############################################
def randomMove(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles. Pour avoir un choix au hasard, il faut
    construire explicitement tous les mouvements. Or, generate_legal_moves() nous donne un iterateur.'''
    return choice([m for m in b.generate_legal_moves()])

def deroulementRandom(b):
    '''Déroulement d'une partie d'échecs au hasard des coups possibles. Cela va donner presque exclusivement
    des parties très longues et sans gagnant. Cela illustre cependant comment on peut jouer avec la librairie
    très simplement.'''
    print("----------")
    print(b)
    if b.is_game_over():
        print("Resultat : ", b.result())
        return
    b.push(randomMove(b))
    deroulementRandom(b)
    b.pop()

def allGames(b, profondeur):
    '''Fonction explore toutes les parties possibles au Morpion. '''
    global nbGames, nbNodes
    nbNodes += 1
    if b.is_game_over():
        nbGames += 1
        return
    if(profondeur > 0):
        for m in b.generate_legal_moves():
            b.push(m)
            allGames(b, profondeur-1)
            b.pop()

def board2tab(b):
    '''Retourne toutes les pieces du jeu ligne par ligne sous forme d'une grande chaine de caractere '''
    return [x.split(' ') for x in str(b).split('\n')]

def evalPiece(p):
    ''' Retourne la valeur de la piece (Selon Shannon)'''
    return valpieces[p]

def evalBoard(b):
    ''' Evalue le jeu selon les valeurs des pièces encore sur le plateau
    Si score > 0, le joueur blanc a plus de valeur sur le plateau, sinon c'est le noir '''
    score = 0
    tab = board2tab(b)
    for l in tab:
        for p in l:
            score += evalPiece(p)
    return score

def MiniMax(b, profondeur, white):
    if profondeur is 0:
        return evalBoard(b)

    if b.is_game_over():
        if b.result() == "1-0":
            if white:
                return 500
            else:
                return -500
        elif b.result() == "0-1":
            if white:
                return -500
            else:
                return 500
        else:
            return 0

    best = -10
    for m in b.generate_legal_moves():
        b.push(m)
        best = max(best, MiniMin(b, profondeur-1, white))
        b.pop()
    return best

def MiniMin(b, profondeur,white):
    if profondeur is 0:
        return evalBoard(b)

    if b.is_game_over():
        if b.result() == "1-0":
            if white:
                return 500
            else:
                return -500
        elif b.result() == "0-1":
            if white:
                return -500
            else:
                return 500
        else:
            return 0
    worst = 10
    for m in b.generate_legal_moves():
        b.push(m)
        worst = min(worst, MiniMax(b, profondeur-1, white))
        b.pop()
    return worst

def MaxAlphaBeta(b, profondeur, alpha, beta, white):
    if profondeur is 0:
        return evalBoard(b)
            
    if b.is_game_over():
        if b.result() == "1-0":
            if white:
                return 500
            else:
                return -500
        elif b.result() == "0-1":
            if white:
                return -500
            else:
                return 500
        else:
            return 0

    for m in b.generate_legal_moves():
        if(time.time() > deadLineReflexion):
            b.pop()
            if(alpha >= beta):
                return beta
            return alpha

        b.push(m)
        alpha = max(alpha, MinAlphaBeta(b, profondeur-1, alpha, beta, white))
    
        if(alpha >= beta):
            b.pop()
            return beta
        b.pop()
    return alpha

def MinAlphaBeta(b, profondeur, alpha, beta, white):
    if profondeur is 0:
        return evalBoard(b)
            
    if b.is_game_over():
        if b.result() == "1-0":
            if white:
                return 500
            else:
                return -500
        elif b.result() == "0-1":
            if white:
                return -500
            else:
                return 500
        else:
            return 0

    for m in b.generate_legal_moves():
        if(time.time() > deadLineReflexion):
            b.pop()
            if(alpha >= beta):
                return alpha
            return beta

        b.push(m)
        beta = min(beta, MaxAlphaBeta(b, profondeur-1, alpha, beta, white))
        if(alpha >= beta):
            b.pop()
            return alpha
        b.pop()
    return beta

def startMiniMax(b, profondeur, white):
    if b.is_game_over():
        return
    bestMove = None
    loss = -1000
    for m in b.generate_legal_moves():
        b.push(m)
        current_loss = MiniMin(b, profondeur, white)
        if(loss < current_loss):
            loss = current_loss
            bestMove = m
        b.pop()
    b.push(bestMove)
    return bestMove

def startAlphaBeta(b, profondeur, white):
    global deadLineReflexion
    if b.is_game_over():
        return
    bestMove = None
    loss = -1000
    alpha = -1000
    beta = 1000
    deadLineReflexion = time.time() + MAX_REFLEXION_TIME

    for m in b.generate_legal_moves():
        if (time.time() > deadLineReflexion):
            b.pop()
            b.push(bestMove)
            return bestMove
        b.push(m)
        current_loss = MinAlphaBeta(b, profondeur, alpha, beta, white)
        if(loss < current_loss):
            loss = current_loss
            bestMove = m
        b.pop()
            
    b.push(bestMove)
    return bestMove

def playGameWithIA(b, profondeur):
    turn = True #True = white
    choice = input('Choose the method: \n\t1. IA use MinMax\n\t2. IA use AlphaBeta\n')
    while(choice != 1 and choice != 2):
        print("Invalid choice !")
        choice = input('Choose the method: \n\t1. IA use MinMax\n\t2. IA use AlphaBeta\n')

    if(choice == 1):
        string = "MINMAX : Choose a configuration: \n\t"
    else:
        string = "ALPHA_BETA : Choose a configuration: \n\t"

    playerStrategy = input(string + '1. IA lvl.' + str(profondeur) + ' vs Random\n\t2. IA lvl.' + str(profondeur) + ' vs IA lvl.1\n\t3. IA (MinMax) lvl.' + str(profondeur) + ' vs IA (AlphaBeta) lvl.' + str(profondeur)+'\n')
    while(playerStrategy != 1 and playerStrategy != 2 and playerStrategy != 3):
        print("Invalid choice !")
        playerStrategy = input(string + '1. IA lvl.' + str(profondeur) + ' vs Random\n\t2. IA lvl.' + str(profondeur) + ' vs IA lvl.1\n\t3. IA (MinMax) lvl.' + str(profondeur) + ' vs IA (AlphaBeta) lvl.' + str(profondeur)+'\n')

    while(not b.is_game_over()):
        print(b)
        print("--------------------------")
        if(turn):
            if(choice == 1):
                print("White (IA MinMax lvl." + str(profondeur) + ") is playing...")
                move = startMiniMax(b, profondeur, turn)
                print("White (IA MinMax lvl." + str(profondeur) + ") played " + str(move))
            elif(choice == 2):
                print("White (IA AlphaBeta lvl." + str(profondeur) + ") is playing...")
                move = startAlphaBeta(b, profondeur, turn)
                print("White (IA AlphaBeta lvl." + str(profondeur) + ") played " + str(move))
            else:
                print("White (IA MinMax lvl." + str(profondeur) + ") is playing...")
                move = startMiniMax(b, profondeur, turn)
                print("White (IA MinMax lvl." + str(profondeur) + ") played " + str(move))

        else:
            if(playerStrategy == 1):
                print("Black (Random) is playing...")
                move = randomMove(b)
                b.push(move)
                print("Black (Random) played " + str(move))
            else:
                if(choice == 1):
                    print("Black (IA MinMax lvl.1) is playing...")
                    move = startMiniMax(b, 1, turn)
                    print("Black (IA MinMax lvl.1) played " + str(move) )
                elif(choice == 2):
                    print("Black (IA AlphaBeta lvl.1) is playing...")
                    move = startAlphaBeta(b, 1, turn)
                    print("Black (IA AlphaBeta lvl.1) played " + str(move) )
                else:
                    print("Black (IA AlphaBeta lvl."+ str(profondeur+3) + ") is playing...")
                    move = startAlphaBeta(b, profondeur+3, turn)
                    print("Black (IA AlphaBeta lvl."+ str(profondeur+3) + ") played " + str(move) )
                
        print("--------------------------")
        turn = not turn

    print("--------------------------")
    if(turn):
        print("Game ended : BLACK WIN !")
    else:
        print("Game ended : WHITE WIN !")
    print("--------------------------")
    print(b)

############################################
############    MAIN PROGRAM   ##############
#############################################
### Deroulement d'une partie aléatoire
#deroulementRandom(board)

### Derouelemnt de toutes les parties (limité à la profondeur)
#allGames(board, PROFONDEUR_MAX)
#print("Nombre de parties : " + str(nbGames))
#print("Nombre de noeuds : " + str(nbNodes))

playGameWithIA(board, PROFONDEUR_MAX)

